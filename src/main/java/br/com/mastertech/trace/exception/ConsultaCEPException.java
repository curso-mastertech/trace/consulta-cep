package br.com.mastertech.trace.exception;

public class ConsultaCEPException extends RuntimeException {
    public ConsultaCEPException(String cnpj) {
        super("Erro ao buscar CEP: " + cnpj);
    }
}
