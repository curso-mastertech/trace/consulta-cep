package br.com.mastertech.trace.controller;

import br.com.mastertech.trace.dto.CEPDTO;
import br.com.mastertech.trace.service.ConsultaCEPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cep")
public class ConsultaCEPController {

    @Autowired
    private ConsultaCEPService consultaCEPService;

    @GetMapping("/{cep}")
    public CEPDTO recebeCadastroEmpresa(@PathVariable String cep) {
        return consultaCEPService.consulta(cep);
    }
}
