package br.com.mastertech.trace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ConsultaCEPApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsultaCEPApplication.class, args);
	}

}
