package br.com.mastertech.trace.service;

import br.com.mastertech.trace.client.ConsultaCEPClient;
import br.com.mastertech.trace.exception.ConsultaCEPException;
import br.com.mastertech.trace.dto.CEPDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConsultaCEPService {

    @Autowired
    private ConsultaCEPClient consultaCEPClient;

    public CEPDTO consulta(String cep) {
        return consultaCEPClient
                .findByCEP(cep)
                .filter(e -> e.getCep() != null)
                .orElseThrow(() -> new ConsultaCEPException(cep));
    }
}
